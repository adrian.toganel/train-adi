package com.tora;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


public class SumTest {
    @Rule
    public ExpectedException expectedExceptionNegative = ExpectedException.none();
    @Test
    public void addPositiveNumbersTest() {
        assertThat(Sum.add(3, 5), is(8));
    }

    @Test
    public void addNegativeNumbersTest2() {
        expectedExceptionNegative.expect(IllegalArgumentException.class);
        expectedExceptionNegative.expectMessage(Sum.LESSTHAN_ERROR_MESSAGE);
        Sum.add(10, -34);
    }

    @Test
    public void addBigIntTest1() {
        expectedExceptionNegative.expect(IllegalArgumentException.class);
        expectedExceptionNegative.expectMessage(Sum.MORETHAN_ERROR_MESSAGE);
        Sum.add((Integer.MAX_VALUE / 2) + 1, 2);
    }


}
