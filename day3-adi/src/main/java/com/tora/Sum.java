package com.tora;

public class Sum {
    static private int max = Integer.MAX_VALUE / 2;
    static private int min = 0;
    static public String LESSTHAN_ERROR_MESSAGE = "None of the parameters should be less than " + min;
    static public String MORETHAN_ERROR_MESSAGE = "None or the parameters should be grater than " + max;

    public static int add(int x, int y) {
        if (x < 0 || y < 0) {
            throw new IllegalArgumentException(LESSTHAN_ERROR_MESSAGE);
        }
        if (x > max || y > max) {
            throw new IllegalArgumentException(MORETHAN_ERROR_MESSAGE);
        }
        return x + y;
    }
}
