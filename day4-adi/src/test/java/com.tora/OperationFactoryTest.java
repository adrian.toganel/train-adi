package com.tora;

import com.tora.exceptions.IncorrectOperationParametersException;
import com.tora.operations.*;
import com.tora.operations.binary.Add;
import com.tora.operations.binary.Divide;
import com.tora.operations.binary.Multiply;
import com.tora.operations.binary.Substract;
import com.tora.operations.unary.Logaritm;
import com.tora.operations.unary.Sin;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

public class OperationFactoryTest {

    @Rule
    public ExpectedException expectedExceptionNegative = ExpectedException.none();

    @Test
    public void plusOperand (){
        assertThat(OperationFactory.getOperation(Operations.ADD, Arrays.asList(1 , 2 )),instanceOf(Add.class));
    }
    @Test
    public void minusOperand (){
        assertThat(OperationFactory.getOperation(Operations.SUBSTRACT, Arrays.asList(1,2 )),instanceOf(Substract.class));
    }
    @Test
    public void multiplyOperand (){
        assertThat(OperationFactory.getOperation( Operations.MULTIPLY,Arrays.asList(1,2)),instanceOf(Multiply.class));
    }
    @Test
    public void divideOperand (){
        assertThat(OperationFactory.getOperation(Operations.DIVIDE,Arrays.asList(1,2)),instanceOf(Divide.class));
    }

    @Test
    public void divideLog (){
        assertThat(OperationFactory.getOperation(Operations.LOG,Arrays.asList(1)),instanceOf(Logaritm.class));
    }

    @Test
    public void divideSin (){
        assertThat(OperationFactory.getOperation(Operations.SIN,Arrays.asList(123)),instanceOf(Sin.class));
    }

    @Test
    public void invalidParamsNumberUnary (){
        expectedExceptionNegative.expect(IncorrectOperationParametersException.class);
        OperationFactory.getOperation(Operations.SIN,Arrays.asList(123,213));
    }

    @Test
    public void invalidParamsNumberBinary (){
        expectedExceptionNegative.expect(IncorrectOperationParametersException.class);
        OperationFactory.getOperation(Operations.ADD,Arrays.asList(123));
    }
}
