package com.tora;

import com.tora.exceptions.IncorrectExpression;
import com.tora.operations.binary.*;
import com.tora.operations.unary.Logaritm;
import com.tora.operations.unary.Sin;
import com.tora.operations.unary.Sqrt;
import com.tora.utils.ExpressionParserUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ExpressionParserTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @Test
    public void invalidTest(){
        String expression = "123sin/23123";
        expectedException.expect(IncorrectExpression.class);
        expectedException.expectMessage("Invalid binary expression: " + expression);
        ExpressionParserUtils.getOperation(expression);
    }

    @Test
    public void validAddExpressionType(){
        assertThat(ExpressionParserUtils.getOperation("230 + -213"),instanceOf(Add.class));
        assertThat(ExpressionParserUtils.getOperation("100+72"),instanceOf(Add.class));
        assertThat(ExpressionParserUtils.getOperation("-100+72"),instanceOf(Add.class));
        assertThat(ExpressionParserUtils.getOperation(" 100 + -172 "),instanceOf(Add.class));
        assertThat(ExpressionParserUtils.getOperation("123.23 + 0.1"),instanceOf(Add.class));
    }

    @Test
    public void validAddExpressionValue(){
        assertThat(ExpressionParserUtils.getOperation("230 + -213").getResult(),is(new BigDecimal(17)));
        assertThat(ExpressionParserUtils.getOperation("100+72").getResult(),is(new BigDecimal("172")));
        assertThat(ExpressionParserUtils.getOperation("-100+72").getResult(),is(new BigDecimal(-28)));
        assertThat(ExpressionParserUtils.getOperation(" 100 + -172 ").getResult(),is(new BigDecimal("-72")));
        assertThat(ExpressionParserUtils.getOperation("123.23 + 0.22").getResult(),is(new BigDecimal(123.45)));
    }

    @Test
    public void validSubstractExpressionType(){
        assertThat(ExpressionParserUtils.getOperation("230 - -213"),instanceOf(Substract.class));
        assertThat(ExpressionParserUtils.getOperation("100-72"),instanceOf(Substract.class));
        assertThat(ExpressionParserUtils.getOperation("-100-72"),instanceOf(Substract.class));
        assertThat(ExpressionParserUtils.getOperation(" 100 - -172 "),instanceOf(Substract.class));
        assertThat(ExpressionParserUtils.getOperation("123.23 - 0.1"),instanceOf(Substract.class));
    }

    @Test
    public void validSubstractExpressionValue(){
        assertThat(ExpressionParserUtils.getOperation("230 - -213").getResult(),is(new BigDecimal("443")));
        assertThat(ExpressionParserUtils.getOperation("100-72").getResult(),is(new BigDecimal(28)));
        assertThat(ExpressionParserUtils.getOperation("-100+-72").getResult(),is(new BigDecimal(-172)));
        assertThat(ExpressionParserUtils.getOperation(" 100 --172 ").getResult(),is(new BigDecimal("272")));
        assertThat(ExpressionParserUtils.getOperation("123.23 - 0.22").getResult(),is(new BigDecimal(123.01)));
    }

    @Test
    public void validMultiplyExpressionType(){
        assertThat(ExpressionParserUtils.getOperation("10 * -213"),instanceOf(Multiply.class));
        assertThat(ExpressionParserUtils.getOperation("100*72"),instanceOf(Multiply.class));
        assertThat(ExpressionParserUtils.getOperation("-100*72"),instanceOf(Multiply.class));
        assertThat(ExpressionParserUtils.getOperation(" 100 * -172 "),instanceOf(Multiply.class));
        assertThat(ExpressionParserUtils.getOperation("123.23 * 0.1"),instanceOf(Multiply.class));
    }

    @Test
    public void validMultiplyExpressionValue(){
        assertThat(ExpressionParserUtils.getOperation("10 * -213").getResult(),is(new BigDecimal(-2130)));
        assertThat(ExpressionParserUtils.getOperation("100*72").getResult(),is(new BigDecimal(7200)));
        assertThat(ExpressionParserUtils.getOperation("-100*-72").getResult(),is(new BigDecimal(7200)));
        assertThat(ExpressionParserUtils.getOperation(" 100 *-172 ").getResult(),is(new BigDecimal("-17200")));
        assertThat(ExpressionParserUtils.getOperation("123 * 0.1").getResult(),is(new BigDecimal(12.3)));
    }

    @Test
    public void validDivideExpressionType(){
        assertThat(ExpressionParserUtils.getOperation("10 / -213"),instanceOf(Divide.class));
        assertThat(ExpressionParserUtils.getOperation("100/72"),instanceOf(Divide.class));
        assertThat(ExpressionParserUtils.getOperation("-100/72"),instanceOf(Divide.class));
        assertThat(ExpressionParserUtils.getOperation(" 100 / -172 "),instanceOf(Divide.class));
        assertThat(ExpressionParserUtils.getOperation("123.23 / 0.1"),instanceOf(Divide.class));
    }

    @Test
    public void validDivideExpressionValue(){
        assertThat(ExpressionParserUtils.getOperation("-230 / -10").getResult(),is(new BigDecimal(23)));
        assertThat(ExpressionParserUtils.getOperation("-100/-50").getResult(),is(new BigDecimal(2)));
        assertThat(ExpressionParserUtils.getOperation(" 100 /-200 ").getResult(),is(new BigDecimal("-0.5")));
        assertThat(ExpressionParserUtils.getOperation("123 / 0.1").getResult(),is(new BigDecimal(1230)));
    }

    @Test
    public void validLogExpressionType(){
        assertThat(ExpressionParserUtils.getOperation("log 100"),instanceOf(Logaritm.class));
        assertThat(ExpressionParserUtils.getOperation("log200"),instanceOf(Logaritm.class));
        assertThat(ExpressionParserUtils.getOperation("log329"),instanceOf(Logaritm.class));
        assertThat(ExpressionParserUtils.getOperation("log -320"),instanceOf(Logaritm.class));
        assertThat(ExpressionParserUtils.getOperation("log0.1"),instanceOf(Logaritm.class));
    }

    @Test
    public void validLogExpressionValue(){
        assertThat(ExpressionParserUtils.getOperation("log145.256").getResult(),is(new BigDecimal(4.978497702968366)));
        assertThat(ExpressionParserUtils.getOperation("log 10").getResult(),is(new BigDecimal(2.302585092994046)));
    }

    @Test
    public void validSinExpressionType(){
        assertThat(ExpressionParserUtils.getOperation("sin 100"),instanceOf(Sin.class));
        assertThat(ExpressionParserUtils.getOperation("sin200"),instanceOf(Sin.class));
        assertThat(ExpressionParserUtils.getOperation("sin329"),instanceOf(Sin.class));
        assertThat(ExpressionParserUtils.getOperation("sin-20"),instanceOf(Sin.class));
        assertThat(ExpressionParserUtils.getOperation("sin0.1"),instanceOf(Sin.class));
    }

    @Test
    public void validSinLogExpressionValue(){
        assertThat(ExpressionParserUtils.getOperation("sin0.66").getResult(),is(new BigDecimal(0.6131168519734338)));
        assertThat(ExpressionParserUtils.getOperation("sin0.23").getResult(),is(new BigDecimal(0.2279775235351884)));
    }

    @Test
    public void validSqrtExpressionType(){
        assertThat(ExpressionParserUtils.getOperation("sqrt 100"),instanceOf(Sqrt.class));
        assertThat(ExpressionParserUtils.getOperation("sqrt200"),instanceOf(Sqrt.class));
        assertThat(ExpressionParserUtils.getOperation("sqrt0.1"),instanceOf(Sqrt.class));
    }

    @Test
    public void validSqrtLogExpressionValue(){
        assertThat(ExpressionParserUtils.getOperation("sqrt100").getResult(),is(new BigDecimal("10")));
        assertThat(ExpressionParserUtils.getOperation("sqrt 1024").getResult(),is(new BigDecimal("32")));
    }

    @Test
    public void validMinExpressionType(){
        assertThat(ExpressionParserUtils.getOperation("min 100, 21"),instanceOf(Min.class));
        assertThat(ExpressionParserUtils.getOperation("min -23, 4"),instanceOf(Min.class));
        assertThat(ExpressionParserUtils.getOperation("min213,-23"),instanceOf(Min.class));
    }

    @Test
    public void validMinLogExpressionValue(){
        assertThat(ExpressionParserUtils.getOperation("min -23, -10").getResult(),is(new BigDecimal("-23")));
        assertThat(ExpressionParserUtils.getOperation("min 45,1024").getResult(),is(new BigDecimal("45")));
    }

    @Test
    public void validMaxExpressionType(){
        assertThat(ExpressionParserUtils.getOperation("max 100, 21"),instanceOf(Max.class));
        assertThat(ExpressionParserUtils.getOperation("max -23, 4"),instanceOf(Max.class));
        assertThat(ExpressionParserUtils.getOperation("max213,-23"),instanceOf(Max.class));
    }

    @Test
    public void validMaxLogExpressionValue(){
        assertThat(ExpressionParserUtils.getOperation("max -23, -10").getResult(),is(new BigDecimal(-10)));
        assertThat(ExpressionParserUtils.getOperation("max 45,1024").getResult(),is(new BigDecimal(1024)));
    }
}
