package com.tora;

import com.tora.operations.binary.Divide;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class DivideOperationTest {
    @Test
    public void testDivide(){
        Divide divideOperation = new Divide<>(Arrays.asList(44.0,22.0));
        assertThat(divideOperation.getResult(),is(new BigDecimal(2.0)));
    }
    @Test
    public void checkValidationFails(){
        Divide addOperation = new Divide<>(Arrays.asList(null,30.0));
        assertFalse(addOperation.isValidInput());
    }
    @Test
    public void checkValidationFailsZeroDivisor(){
        Divide addOperation = new Divide<>(Arrays.asList(120.0,0.0));
        assertFalse(addOperation.isValidInput());
    }
    @Test
    public void checkValidationPass(){
        Divide addOperation = new Divide<>(Arrays.asList(0.0,23130.0));
        assertTrue(addOperation.isValidInput());
    }

    @Test
    public void checkInvalidMessage(){
        Divide addOperation = new Divide<>(Arrays.asList(0.0,23130.0));
        assertThat(addOperation.getInvalidOperationReason(),is("Cannot divide by 0"));
    }
}
