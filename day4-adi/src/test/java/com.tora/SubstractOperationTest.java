package com.tora;

import com.tora.operations.binary.Substract;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SubstractOperationTest {
    @Test
    public void substractTet (){
        Substract subs = new Substract<>(Arrays.asList(10.0 ,-40.0));
        assertThat(subs.getResult(),is(new BigDecimal(50.0)));
    }
}
