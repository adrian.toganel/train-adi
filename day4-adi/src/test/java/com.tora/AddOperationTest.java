package com.tora;

import com.tora.operations.binary.Add;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class AddOperationTest {
    @Test
    public void testAdd(){
        Add addOperation = new Add<>(Arrays.asList(10.0,30.0));
        assertThat(addOperation.getResult(),is(new BigDecimal(40)));
    }
    @Test
    public void checkValidationFails(){
        Add addOperation = new Add<>(Arrays.asList(null,30.0));
        assertThat(addOperation.isValidInput(),is(false));
    }
    @Test
    public void checkValidationPass(){
        Add addOperation = new Add<>(Arrays.asList(-2.0,23130.0));
        assertThat(addOperation.isValidInput(),is(true));
    }
}
