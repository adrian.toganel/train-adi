package com.tora;

import com.tora.operations.binary.Multiply;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MultiplyOperationTest {
    @Test
    public void testMultiply(){
        Multiply multiplyOperation = new Multiply<>(Arrays.asList(10.0,22.0));
        assertThat(multiplyOperation.getResult(),is(new BigDecimal(220.0)));
    }
}
