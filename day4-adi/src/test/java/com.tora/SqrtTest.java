package com.tora;

import com.tora.operations.unary.Sqrt;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class SqrtTest {
    @Test
    public void testSqrt(){
        Sqrt addOperation = new Sqrt<>(100);
        assertThat(addOperation.getResult(),is(BigDecimal.valueOf(10)));
    }
    @Test
    public void testNegativeSqrt(){
        Sqrt addOperation = new Sqrt(Arrays.asList(-10));
        assertFalse(addOperation.isValidInput());
    }
    @Test
    public void checkValidationPass(){
        Sqrt addOperation = new Sqrt<>(Arrays.asList(12300));
        assertTrue(addOperation.isValidInput());
    }
}
