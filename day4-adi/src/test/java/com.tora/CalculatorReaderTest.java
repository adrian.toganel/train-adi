package com.tora;

import com.tora.utils.ExpressionValidators;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CalculatorReaderTest {

    @Test
    public void testBinaryExpression() {
        assertTrue(ExpressionValidators.isValidBinaryEpression("-123 +2"));
        assertTrue(ExpressionValidators.isValidBinaryEpression("123-2"));
        assertTrue(ExpressionValidators.isValidBinaryEpression("23.4 * 2"));
        assertTrue(ExpressionValidators.isValidBinaryEpression("-123  / 2"));
        assertTrue(ExpressionValidators.isValidBinaryEpression(" -123 +2"));
        assertTrue(ExpressionValidators.isValidBinaryEpression(" 123+-2"));
    }

    @Test
    public void testUnaryExpression() {
        assertTrue(ExpressionValidators.isValidUnaryEpression("log 213"));
        assertTrue(ExpressionValidators.isValidUnaryEpression("log123"));
        assertTrue(ExpressionValidators.isValidUnaryEpression("sin -234"));
        assertTrue(ExpressionValidators.isValidUnaryEpression("log32"));
        assertTrue(ExpressionValidators.isValidUnaryEpression("log0"));
        assertTrue(ExpressionValidators.isValidUnaryEpression("sin123"));
    }

    @Test
    public void testInvalidBynaryExpression() {
        assertFalse(ExpressionValidators.isValidBinaryEpression("a123-*2"));
        assertFalse(ExpressionValidators.isValidBinaryEpression("/123+3"));
        assertFalse(ExpressionValidators.isValidBinaryEpression("a-b"));
        assertFalse(ExpressionValidators.isValidBinaryEpression(" - 12 & 3 +2"));
    }

    @Test
    public void testInvalidUnaryExpression() {
        assertFalse(ExpressionValidators.isValidUnaryEpression("-a123 +2"));
        assertFalse(ExpressionValidators.isValidUnaryEpression("213log213"));
        assertFalse(ExpressionValidators.isValidUnaryEpression("sinlog123"));
        assertFalse(ExpressionValidators.isValidUnaryEpression("2+log3"));
        assertFalse(ExpressionValidators.isValidUnaryEpression("29log2"));
    }
}
