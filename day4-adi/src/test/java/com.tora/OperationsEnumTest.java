package com.tora;

import com.tora.operations.Operations;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class OperationsEnumTest {
    @Test
    public void getOperation(){
        assertThat(Operations.getOperation("-"),is(Operations.SUBSTRACT));
    }
    @Test
    public void getOperationNull(){
        Assert.assertNull(Operations.getOperation("$"));
    }
    @Test
    public void isBinaryTest(){
        Assert.assertTrue(Operations.ADD.isBinary());
        Assert.assertFalse(Operations.LOG.isBinary());
    }
    @Test
    public void isUnaryTest(){
        Assert.assertTrue(Operations.LOG.isUnary());
        Assert.assertTrue(Operations.SIN.isUnary());
    }

    @Test
    public void detectPatternAdd()
    {
        assertThat(Operations.getOperationFromExpression("-123 + 123"), is(Operations.ADD));
        assertThat(Operations.getOperationFromExpression(" -123 + 123"), is(Operations.ADD));
        assertThat(Operations.getOperationFromExpression(" 23.0 + - 100"), is(Operations.ADD));
        assertThat(Operations.getOperationFromExpression("23.0+100"), is(Operations.ADD));
    }

    @Test
    public void detectPatternSubstract()
    {
        assertThat(Operations.getOperationFromExpression("-123 - 123"), is(Operations.SUBSTRACT));
        assertThat(Operations.getOperationFromExpression(" -123 - 123"), is(Operations.SUBSTRACT));
        assertThat(Operations.getOperationFromExpression(" 23.0 - 100"), is(Operations.SUBSTRACT));
        assertThat(Operations.getOperationFromExpression("23.0-100"), is(Operations.SUBSTRACT));
    }

    @Test
    public void detectPatternMultiply(){
        assertThat(Operations.getOperationFromExpression(" -3  * - 100"), is(Operations.MULTIPLY));
        assertThat(Operations.getOperationFromExpression("3.6*4"), is(Operations.MULTIPLY));
        assertThat(Operations.getOperationFromExpression("29.67 *-2 "), is(Operations.MULTIPLY));
    }

    @Test
    public void detectPatternDivide(){
        assertThat(Operations.getOperationFromExpression(" -3  / - 100"), is(Operations.DIVIDE));
        assertThat(Operations.getOperationFromExpression("3.6/4"), is(Operations.DIVIDE));
        assertThat(Operations.getOperationFromExpression("29.67 /-2 "), is(Operations.DIVIDE));
    }
    @Test
    public void detectPatternSin(){
        assertThat(Operations.getOperationFromExpression("sin-3"), is(Operations.SIN));
        assertThat(Operations.getOperationFromExpression("sin 1000"), is(Operations.SIN));
        assertThat(Operations.getOperationFromExpression("sin -20"), is(Operations.SIN));
    }
    @Test
    public void detectPatternLog(){
        assertThat(Operations.getOperationFromExpression("log -3"), is(Operations.LOG));
        assertThat(Operations.getOperationFromExpression("log100"), is(Operations.LOG));
        assertThat(Operations.getOperationFromExpression("log 123 "), is(Operations.LOG));
    }


}
