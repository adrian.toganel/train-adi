package com.tora.utils;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ExpressionMultiParserTest {
    @Test
    public void calculateTest(){
        assertThat(new ExpressionParser("20+11-3*5-5").compute(),is(new BigDecimal(11.0)));
        assertThat(new ExpressionParser("20-3+19-19+3+5*5-50/2-4*4").compute(),is(new BigDecimal(4.0)));
        assertThat(new ExpressionParser("5*5-21+22-9/3").compute(),is(new BigDecimal(23.0)));
        assertThat(new ExpressionParser("20-3+19-19+3+5*5-50/2-4*4").compute(),is(new BigDecimal(4.0)));
        assertThat(new ExpressionParser("20-3+19-19+3+5*5-50/2-4*4").compute(),is(new BigDecimal(4.0)));
    }
}
