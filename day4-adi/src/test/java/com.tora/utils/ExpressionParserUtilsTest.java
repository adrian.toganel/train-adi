package com.tora.utils;

import com.tora.operations.IOperantion;
import com.tora.operations.binary.Add;
import com.tora.operations.binary.Divide;
import com.tora.operations.binary.Multiply;
import com.tora.operations.binary.Substract;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ExpressionParserUtilsTest {
    @Test
    public void testAddBynaryExpression() {
        IOperantion op = ExpressionParserUtils.getOperation("223+-123");
        assertThat(op,instanceOf(Add.class));
        assertThat(op.getResult(), is(new BigDecimal(100.0)));
    }
    @Test
    public void testSubBynaryExpression() {
        IOperantion op = ExpressionParserUtils.getOperation(" 100- 50");
        assertThat(op,instanceOf(Substract.class));
        assertThat(op.getResult(), is(new BigDecimal(50.0)));
    }
    @Test
    public void testMultiplyBynaryExpression() {
        IOperantion op = ExpressionParserUtils.getOperation("-10 * 20");
        assertThat(op,instanceOf(Multiply.class));
        assertThat(op.getResult(), is(new BigDecimal(-200.0)));
    }
    @Test
    public void testDivBynaryExpression() {
        IOperantion op = ExpressionParserUtils.getOperation("20 / 40");
        assertThat(op,instanceOf(Divide.class));
        assertThat(op.getResult(), is(new BigDecimal(0.5)));
    }
}
