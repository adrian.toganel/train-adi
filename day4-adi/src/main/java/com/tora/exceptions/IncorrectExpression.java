package com.tora.exceptions;

public class IncorrectExpression extends IllegalArgumentException {
    private static final String DEFAULT_MESSAGE = "Incorrect expression";

    public IncorrectExpression(String s) {
        super(s);
    }

    public IncorrectExpression() {
        super(DEFAULT_MESSAGE);
    }
}
