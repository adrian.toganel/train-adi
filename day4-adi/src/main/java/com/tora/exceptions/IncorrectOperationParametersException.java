package com.tora.exceptions;

public class IncorrectOperationParametersException extends IllegalArgumentException {
    private static final String DEFAULT_MESSAGE = "For binary operations we need exactly two parameters, for unary we need exactly one parameter";

    public IncorrectOperationParametersException(String s) {
        super(s);
    }

    public IncorrectOperationParametersException() {
        super(DEFAULT_MESSAGE);
    }
}
