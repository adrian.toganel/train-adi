package com.tora.utils;

import com.tora.operations.Operations;

import java.io.BufferedReader;
import java.io.IOException;

import static com.tora.utils.ExpressionValidators.isValidExpression;

public class InputReader {
    public static final String EXIT_COMMAND = "exit";
    public static final String HISTORY_COMMAND = "history";
    public static final String USAGE_STRING = "The following operations are supported: \n" + Operations.exampleString;
    BufferedReader reader;

    public InputReader(BufferedReader reader) {
        this.reader = reader;
    }

    public String readValidExpression() throws IOException {
        while (true) {
            System.out.println("Please enter your expression: \n ");
            String line = reader.readLine();
            if (isValidExpression(line) || EXIT_COMMAND.equals(line.trim()) || HISTORY_COMMAND.equals(line.trim()))
                return line;
            System.out.println("Invalid expression: " + line);
            System.out.println( USAGE_STRING );
        }

    }
}
