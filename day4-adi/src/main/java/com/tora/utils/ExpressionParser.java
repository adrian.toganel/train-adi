package com.tora.utils;

import com.tora.exceptions.IncorrectOperationParametersException;
import com.tora.operations.OperationFactory;
import com.tora.operations.Operations;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Stack;

/**
 * This class can be used to compute arithmetic expression
 * The arithmetic expression is received in constructor and method compute can be called to get the result of the operation
 */
public class ExpressionParser {
    private String expression;

    public ExpressionParser(String expression) {
        this.expression = expression;
    }

    /**
     *
     * @return the value of the expression
     */
    public Number compute(){
        // have two stacks - one for numbers and open for operators
        Stack<Double> numberStack = new Stack<Double>();
        Stack<Operations> operatorStack = new Stack<Operations>();

        //walk to the expression and try to isolate numbers and operands
        for(int i = 0; i < expression.length(); i++){
            try{
                int number = parseNumber(i);
                numberStack.push(Double.valueOf(number));

                //the position at the end of last number
                i += Integer.toString(number).length();

                // if all chars in the string were checked exit
                if(i >= expression.length()){
                    break;
                }

                Operations op = parseOperator(i);

                collapseTop(numberStack, operatorStack, op);
                operatorStack.push(op);
            } catch(NumberFormatException ex){
                return Integer.MIN_VALUE;
            }
        }
        collapseTop(numberStack, operatorStack, Operations.NONE);
        if(numberStack.size() == 1 && operatorStack.size() == 0){
            return new BigDecimal(numberStack.pop());
        }
        return 0;

    }

    /**
     * It is used to update number and operator stack (the stacks can have after execution of this code maximum two numbers in the stack and a low priority operator in the operators stack)
     *
     * @param numberStack - the numbers stack
     * @param operatorStack - the operator stack
     * @param futureTop - the current operator
     */
    private void collapseTop(Stack<Double> numberStack, Stack<Operations> operatorStack, Operations futureTop){
        while(numberStack.size() >= 2 && operatorStack.size() >= 1){
            if(futureTop.getArithmeticPriority() <= operatorStack.peek().getArithmeticPriority()){
                computeTop(numberStack, operatorStack);
            } else{
                break;
            }
        }
    }

    /**
     * It computes replaces first two entries in the numbers stack with the result of the operation between those two using the first operand in operator stack
     * @param numberStack after running this the first two  elements will be replaced the result of the arithmetic  operation of them using the top operand from  operatorStack
     * @param operatorStack
     */
    private void computeTop(Stack<Double> numberStack, Stack<Operations> operatorStack) {
        double second = numberStack.pop();
        double first = numberStack.pop();
        Operations op = operatorStack.pop();
        double result = OperationFactory.getOperation(op, Arrays.asList(first,second)).getResult().doubleValue();
        numberStack.push(result);
    }

    /**
     * This is used to determine the first arithmetic operator in expression starting with specific position
     * @param offset the position
     * @return the operation
     */
    private Operations parseOperator(int offset) {
        char op = 0;
        if(offset < expression.length()) {
            op = expression.charAt(offset);
        }
        Operations result = Operations.getOperation(String.valueOf(op));
        if (result == null){
            throw new IncorrectOperationParametersException("Operand " + op + " not recongnized ");
        }
        return result;
    }

    /**
     * This is used to determine the first number from "expression" starting with a specific offset
     * ie for expression = "123+456+6789+10" and offset=8 the returned value will be 6789
     * @param offset the index in the expression where the search should begin
     * @return the number that starts on position offset and end at the first non numeric char
     */
    private int parseNumber (int offset){
        StringBuilder sb = new StringBuilder();
        while(offset < expression.length() && Character.isDigit(expression.charAt(offset))){
            sb.append(expression.charAt(offset));
            offset++;
        }
        return Integer.parseInt(sb.toString());
    }
}
