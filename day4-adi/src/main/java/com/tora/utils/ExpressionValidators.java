package com.tora.utils;

import com.tora.operations.Operations;
import org.apache.commons.lang3.math.NumberUtils;

import static com.tora.operations.Operations.binarySigns;


/**
 * Class that provides static util methods to validate input and default messages for user comunication
 */

public class ExpressionValidators {


    public static boolean isValidUnaryEpression(String input) {
        Operations op = Operations.getOperationFromExpression(input);
        return isValidUnaryEpression(op);
    }

    private static boolean isValidUnaryEpression(Operations op) {
        return (Operations.NONE != op && op.isUnary());
    }

    public static boolean isValidExpression(String input){
        return isValidBinaryEpression(input) || isValidUnaryEpression(input);
    }

    public static boolean isValidBinaryEpression(String input) {
        Operations op = Operations.getOperationFromExpression(input);
        return isValidBinaryEpression(op);
    }

    private static boolean isValidBinaryEpression(Operations op) {
        return (Operations.NONE != op && op.isBinary());
    }

}
