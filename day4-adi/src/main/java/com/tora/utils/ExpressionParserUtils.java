package com.tora.utils;

import com.tora.exceptions.IncorrectExpression;
import com.tora.operations.IOperantion;
import com.tora.operations.OperationFactory;
import com.tora.operations.Operations;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.tora.utils.ExpressionValidators.isValidBinaryEpression;
import static com.tora.utils.ExpressionValidators.isValidUnaryEpression;


public class ExpressionParserUtils {
    public static String NUMBERS_REGEXP = "-?\\d+(\\.\\d*)?";

    /**
     * Can be used to determine the correct operation and parameters from user input
     * @param inputExpression - user input string
     * @return the Operation object with the numeric parameters populated
     */
    public static IOperantion getOperation(String inputExpression) {
        Operations op;

        //remove white spaces
        String expression = inputExpression.replaceAll(" ", "");
        if (!isValidBinaryEpression(expression) && !isValidUnaryEpression(expression)) {
            throw new IncorrectExpression("Invalid binary expression: " + expression);
        } else {
            op = Operations.getOperationFromExpression(expression);
        }

        return OperationFactory.getOperation(op, extractNumberFromExpression(expression));
    }

    /**
     * All the numbers from expression are extracted and populated in a list that can be used as input for Operation
     * @param expression user input string
     * @return a Lsit with all distinct numbers in expression
     */
    private static List<BigDecimal> extractNumberFromExpression(String expression) {
        Pattern r = Pattern.compile(NUMBERS_REGEXP);
        Matcher m = r.matcher(expression);
        List<BigDecimal> numberArguments = new ArrayList<>();
        while (m.find()) {
            String groupString = m.group();
            int indexOfGroup = expression.indexOf(groupString);
            if (numberArguments.size() > 0 && groupString.startsWith(Operations.SUBSTRACT.getSign()) &&
                    Operations.getOperation(expression.substring(indexOfGroup - 1, indexOfGroup)) == null &&
                    ',' != expression.charAt(indexOfGroup-1)) {
                //in case we are in an substract expression with the second parameter a positive number 12-34
                numberArguments.add(new BigDecimal(groupString.replace("-", "")));
            } else {
                // in case this is not a substract or is a substract that has the second number a negative one with
                numberArguments.add(new BigDecimal(groupString));
            }
        }
        return numberArguments;
    }
}
