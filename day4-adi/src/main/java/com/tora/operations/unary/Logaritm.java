package com.tora.operations.unary;

import java.math.BigDecimal;
import java.util.List;

public class Logaritm<T extends Number> extends AbstractUnaryOperation<T>{


    public Logaritm(List<T> a) {
        super(a);
    }

    protected Logaritm(T a) {
        super(a);
    }

    @Override
    public BigDecimal getResult() {
        return new BigDecimal(Math.log(getFirst().doubleValue()));
    }
}
