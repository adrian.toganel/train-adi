package com.tora.operations.unary;

import java.math.BigDecimal;
import java.util.List;

public class Sin <T extends Number> extends AbstractUnaryOperation<T>{

    public Sin(List<T> a) {
        super(a);
    }

    public Sin(T a) {
        super(a);
    }

    @Override
    public BigDecimal getResult() {
        return new BigDecimal(Math.sin(getArgs().get(0).doubleValue()));
    }
}
