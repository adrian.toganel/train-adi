package com.tora.operations.unary;

import java.math.BigDecimal;
import java.util.List;

public class Sqrt<T extends Number> extends AbstractUnaryOperation<T>{

    public Sqrt(List<T> a) {
        super(a);
    }

    public Sqrt(T a) {
        super(a);
    }

    @Override
    public boolean isValidInput() {
        return getFirst() != null && getFirst().doubleValue() > 0;
    }

    @Override
    public BigDecimal getResult() {
        return new BigDecimal(Math.sqrt(getArgs().get(0).doubleValue()));
    }
}
