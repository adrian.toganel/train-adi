package com.tora.operations.unary;

import com.tora.operations.IOperantion;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractUnaryOperation <T extends Number> implements IOperantion {
    private List<T> numberArgsList = new ArrayList<>();
    private String DEFAULT_ERROR = "Argument must be non-null and non-empty";

    protected AbstractUnaryOperation(List<T> a) {
        this.numberArgsList.addAll(a);
    }

    protected AbstractUnaryOperation(T a) {
        this.numberArgsList.add(a);
    }

    protected List<T> getArgs() {
        return numberArgsList;
    }

    public T getFirst(){
        return numberArgsList.get(0);
    }

    public T getSecond(){
        return numberArgsList.get(1);
    }


    @Override
    public boolean isValidInput() {
        return numberArgsList != null && numberArgsList.size() == 1;
    }

    @Override
    public String getInvalidOperationReason() {
        return DEFAULT_ERROR;
    }
}
