package com.tora.operations.binary;

import java.math.BigDecimal;
import java.util.List;

public class Multiply<T extends Number>  extends AbstractBinaryOperation<T> {


    public Multiply(List<T> a) {
        super(a);
    }

    public Multiply(T a) {
        super(a);
    }

    @Override
    public BigDecimal getResult() {
        return new BigDecimal(getFirst().doubleValue() * getSecond().doubleValue());
    }
}
