package com.tora.operations.binary;

import java.math.BigDecimal;
import java.util.List;

public class Max<T extends Number> extends AbstractBinaryOperation<T> {
    public Max(List<T> a) {
        super(a);
    }

    protected Max(T a) {
        super(a);
    }

    @Override
    public BigDecimal getResult() {
        return new BigDecimal(getFirst().doubleValue() < getSecond().doubleValue() ? getSecond().doubleValue() : getFirst().doubleValue());
    }
}
