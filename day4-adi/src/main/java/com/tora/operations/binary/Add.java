package com.tora.operations.binary;

import java.math.BigDecimal;
import java.util.List;

public class Add<T extends Number> extends AbstractBinaryOperation<T> {


    public Add(List<T> a) {
        super(a);
    }

    protected Add(T a) {
        super(a);
    }

    @Override
    public BigDecimal getResult() {
        return new BigDecimal(getFirst().doubleValue() + getSecond().doubleValue());
    }
}
