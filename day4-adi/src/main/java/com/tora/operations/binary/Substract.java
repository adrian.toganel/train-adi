package com.tora.operations.binary;

import java.math.BigDecimal;
import java.util.List;

public class Substract <T extends Number>  extends AbstractBinaryOperation<T>{

    public Substract(List<T> a) {
        super(a);
    }

    protected Substract(T a) {
        super(a);
    }

    @Override
    public BigDecimal getResult() {
        return new BigDecimal(getFirst().doubleValue() - getSecond().doubleValue());
    }
}
