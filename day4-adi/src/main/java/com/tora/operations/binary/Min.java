package com.tora.operations.binary;

import java.math.BigDecimal;
import java.util.List;

public class Min<T extends Number> extends AbstractBinaryOperation<T> {
    public Min(List<T> a) {
        super(a);
    }

    protected Min(T a) {
        super(a);
    }

    @Override
    public BigDecimal getResult() {
        return new BigDecimal(getFirst().doubleValue() > getSecond().doubleValue() ? getSecond().doubleValue() : getFirst().doubleValue());
    }
}
