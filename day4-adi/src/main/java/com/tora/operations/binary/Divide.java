package com.tora.operations.binary;

import java.math.BigDecimal;
import java.util.List;

public class Divide<T extends Number>  extends AbstractBinaryOperation<T> {

    private String DEFAULT_ERROR = "Cannot divide by 0";

    public Divide(List<T> a) {
        super(a);
    }

    protected Divide(T a) {
        super(a);
    }

    @Override
    public BigDecimal getResult() {
        return new BigDecimal(getFirst().doubleValue() / getSecond().doubleValue());
    }

    @Override
    public boolean isValidInput() {
        return super.isValidInput() && getSecond().doubleValue() != 0;
    }

    @Override
    public String getInvalidOperationReason() {
        return DEFAULT_ERROR;
    }
}
