package com.tora.operations.binary;

import com.tora.operations.unary.AbstractUnaryOperation;

import java.util.List;

/**
 * It contains the common parts of each operation  ie. numbers initialization, default parameter validation
 */

public abstract class AbstractBinaryOperation <T extends Number> extends AbstractUnaryOperation<T>  {
    private static final String DEFAULT_ERROR  = "Both number must be non-null";

    protected AbstractBinaryOperation(List a) {
        super(a);
    }

    protected AbstractBinaryOperation(T a) {
        super(a);
    }

    @Override
    public boolean isValidInput() {
        return getArgs().size() == 2 && getFirst() != null && getSecond() != null;
    }

    @Override
    public String getInvalidOperationReason() {
        return DEFAULT_ERROR;
    }
}
