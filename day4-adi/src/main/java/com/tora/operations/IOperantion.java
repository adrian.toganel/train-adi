package com.tora.operations;


/**
 * public interface that should be implemented by all operations from calculator (add, substract, etc)
 */
public interface IOperantion {
    <T extends Number> T getResult();
    boolean isValidInput();
    String getInvalidOperationReason();
}
