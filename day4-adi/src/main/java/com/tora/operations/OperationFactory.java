package com.tora.operations;

import com.tora.exceptions.IncorrectOperationParametersException;
import com.tora.operations.binary.*;
import com.tora.operations.unary.Logaritm;
import com.tora.operations.unary.Sin;
import com.tora.operations.unary.Sqrt;

import java.util.List;

/**
 * This Factory class can be used to create return the specific object based on the operand parameter received
 */
public class OperationFactory {

    public static <T extends Number> IOperantion getOperation(Operations operand, List<T> arguments) {
        if (operand.getRequiredParameterNumber() != arguments.size()){
            throw new IncorrectOperationParametersException("Incorrect parameter number for operand" + operand.getSign()+
            " required " +operand.getRequiredParameterNumber()+ ", provided:" + arguments);
        }
        switch (operand) {
            case ADD:
                return new Add<>(arguments);
            case SUBSTRACT:
                return new Substract<>(arguments);
            case MULTIPLY:
                return new Multiply<>(arguments);
            case DIVIDE:
                return new Divide<>(arguments);
            case MIN:
                return new Min<>(arguments);
            case MAX:
                return new Max<>(arguments);
            case LOG:
                return new Logaritm<>(arguments);
            case SQRT:
                return new Sqrt<>(arguments);
            case SIN:
                return new Sin<>(arguments);
            default:
                throw new IncorrectOperationParametersException("Unknown operand : " + operand.getSign());
        }

    }
}
