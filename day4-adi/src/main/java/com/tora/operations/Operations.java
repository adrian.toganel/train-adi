package com.tora.operations;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * It describes an Operation and it has
 * Operation name - add, divide, sin, etc
 * Number of parameters (ie Add needs two numeric parms, log needs only 1)
 * The computation priority, it is used for multiple expression in order to determine which is the orderp of operation, ie first executie unary operations log\sin then multiply and divide and the add and substract are the last that are executed
 * RegularExpression when reading from command line
 */
public enum Operations {
    ADD("+", 2, 1, "^-?\\d+(\\.\\d*)?[+]-?\\d+(\\.\\d*)?","10.5+123 or -45 + 23.12"),
    SUBSTRACT("-", 2, 1, "^-?\\d+(\\.\\d*)?[-]-?\\d+(\\.\\d*)?","923 - 31.4"),
    DIVIDE("/", 2, 2,"^-?\\d+(\\.\\d*)?[/]-?\\d+(\\.\\d*)?","-231 / 17.2"),
    MULTIPLY("*", 2, 2,"^-?\\d+(\\.\\d*)?[*]-?\\d+(\\.\\d*)?","123.5 * 92 or 23 *-10 "),
    MIN("min", 2, 3,"^min-?\\d+(\\.\\d*)?[,]-?\\d+(\\.\\d*)?","min -342.3, 293"),
    MAX("max", 2, 3,"^max-?\\d+(\\.\\d*)?[,]-?\\d+(\\.\\d*)?", "mac 93 , -239"),
    LOG("log", 1, 3, "^log-?\\d+(\\.\\d*)?","log 123.4"),
    SIN("sin", 1, 3,"^sin-?\\d+(\\.\\d*)?", "sin 0.12345"),
    SQRT("sqrt", 1, 3,"^sqrt\\d+(\\.\\d*)?","sqrt 256"),
    NONE("none", 0, 0,"","");

    // this is the string that is used to recognize the operation
    private final String sign;
    // this is needed to validate the number of parameters 2 for binary operation and 1 for unary
    private int requiredParameterNummber = 2;
    //the priority for multiple expression (low prio Add - value 2 and higher prio Multiply - value 1)
    private int arithmeticPriority = 2;
    //regular expression pattern
    private String regularExpressionPattern = "";
    //operation example
    private String example = "";


    public static String binarySigns = "";
    public static String unarySigns = "";
    public static String unarySignsRegExp = "";
    public static String binarySignsRegExp = "";
    public static String exampleString = "";


    Operations(String sign, int parameterNumber, int arithmeticPriority, String regularExpressionPattern, String example) {
        this.sign = sign;
        this.requiredParameterNummber = parameterNumber;
        this.arithmeticPriority = arithmeticPriority;
        this.regularExpressionPattern = regularExpressionPattern;
        this.example = example;
    }

    public String getExample() {
        return example;
    }

    public static Operations getOperationFromExpression(String expression) {
        // remove spaces
        String s = expression.replaceAll(" ","");
        for (Operations op : values()){
            Pattern r = Pattern.compile(op.getRegularExpressionPattern());
            Matcher m = r.matcher(s);
            boolean found = m.find();
            if (found) return op;
        }

        return Operations.NONE;
    }

    public String getRegularExpressionPattern() {
        return regularExpressionPattern;
    }

    public String getSign() {
        return sign;
    }

    public int getArithmeticPriority() {
        return arithmeticPriority;
    }

    public boolean isBinary() {
        return getRequiredParameterNumber() == 2;
    }

    public boolean isUnary() {
        return getRequiredParameterNumber() == 1;
    }

    public int getRequiredParameterNumber() {
        return requiredParameterNummber;
    }

    public static Operations getOperation(String sign) {
        for (Operations op : values()) {
            if (op.getSign().equals(sign)) return op;
        }
        return null;
    }

    static {
        for (Operations sign : values()) {
            if (sign.isBinary()) {
                binarySigns = binarySigns + sign.getSign();
                if (binarySignsRegExp.length() > 0) {
                    binarySignsRegExp = binarySignsRegExp + "|" + sign.getSign();
                } else {
                    binarySignsRegExp = binarySignsRegExp + sign.getSign();
                }

            } else if (sign.isUnary()) {
                unarySigns = unarySigns + sign.getSign();
                if (unarySignsRegExp.length() > 0) {
                    unarySignsRegExp = unarySignsRegExp + "|" + sign.getSign();
                } else {
                    unarySignsRegExp = unarySignsRegExp + sign.getSign();
                }
            }
            exampleString = exampleString + "\n" + sign.name() + ": " + sign.example;
        }
    }
}
