package com.tora.operations;

import java.math.BigDecimal;

public class HistoryEntry {
    private Operations operations;
    private String expression;
    private BigDecimal result;
    private String time;

    public HistoryEntry(Operations operations, String expression, BigDecimal result, String time) {
        this.operations = operations;
        this.expression = expression;
        this.result = result;
        this.time = time;
    }

    @Override
    public String toString() {
        return "HistoryEntry{" +
                ", time='" + time + '\'' +
                ",operations=" + operations +
                ", expression='" + expression + '\'' +
                ", result=" + result +
                '}';
    }
}
