package com.tora;


import com.tora.operations.HistoryEntry;
import com.tora.operations.Operations;
import com.tora.utils.ExpressionParserUtils;
import com.tora.utils.InputReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.*;

/**
 * Main class that is used to read the input values.
 */

public class CalculatorMain {


    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private static List<HistoryEntry> history = new ArrayList<>();
    private static int operationsNumber = 0;
    private static final InputReader inputReader = new InputReader(reader);

    public static void main(String[] args) throws IOException {
        System.out.println("Hello");
        System.out.println(InputReader.USAGE_STRING);

        while (true) {

            if (runCalculator()) return;
        }
    }

    private static boolean runCalculator() throws IOException {
        System.out.println("*********Operation #" + ++operationsNumber + " *******");
        String expression = inputReader.readValidExpression();
        if (InputReader.EXIT_COMMAND.equals(expression)) {
            return true;
        }
        proccessExpression(history, expression);
        return false;
    }

    private static void proccessExpression(List<HistoryEntry> history, String expression) {

        if (InputReader.HISTORY_COMMAND.equals(expression)) {
            System.out.println(history);
            return;
        }

        BigDecimal resultNumber = ExpressionParserUtils.getOperation(expression).getResult();
        String result = "Result is: " + expression + " = " + resultNumber;
        System.out.println(result);

        //update history
        history.add( new HistoryEntry(Operations.getOperationFromExpression(expression), expression, resultNumber, java.time.Clock.systemUTC().instant().toString()));
    }

}
