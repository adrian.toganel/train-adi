package com.tora;

import com.tora.multikeymap.ComposedKey;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class ComposedKeyTest {
    @Test
    public void equalsTestInt(){
        ComposedKey<Integer> key1 = new ComposedKey<>(12,-23,43);
        ComposedKey<Integer> key2 = new ComposedKey<>(12,-23,43);
        assertThat(key1,is(key2));
    }
    @Test
    public void equalsTestString(){
        ComposedKey<String> key1 = new ComposedKey<>("a","b","c");
        ComposedKey<String> key2 = new ComposedKey<>("a","b","c");
        assertThat(key1,is(key2));
    }

    @Test
    public void notEqualsTestInt(){
        ComposedKey<Integer> key1 = new ComposedKey<>(12,-23,43);
        ComposedKey<Integer> key2 = new ComposedKey<>(12,23,43);
        assertThat(key1,not(key2));
    }

    @Test public void notEqualsDiffTypeTest(){
        ComposedKey<String> key1 = new ComposedKey<>("1","2","3");
        ComposedKey<Integer> key2 = new ComposedKey<>(1,2,3);
        assertThat(key1,not(key2));
    }

    @Test public void notEqualsPartialTest(){
        ComposedKey<String> key1 = new ComposedKey<>("a","b","c");
        ComposedKey<String> key2 = new ComposedKey<>("a","b");
        assertThat(key1,not(key2));
    }

}
