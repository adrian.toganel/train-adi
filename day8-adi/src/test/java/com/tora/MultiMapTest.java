package com.tora;

import com.tora.multikeymap.ComposedKey;
import com.tora.multikeymap.MultiMap;
import org.junit.Test;


import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class MultiMapTest {
    @Test
    public void sizePutContainsTest() {
        MultiMap<String, String> map = new MultiMap<>();
        String value = "TestPut";
        String k1 = "k1";
        String k2 = "k2";
        map.putEntry(value, k1, k2);
        assertThat(map.containsKey(k1, k2), is(true));
        assertThat(map.containsValue(value), is(true));

    }

    @Test
    public void clearTest() {
        MultiMap<String, String> map = new MultiMap<>();
        String value = "TestPut";
        String k1 = "k1";
        String k2 = "k2";
        map.putEntry(value, k1, k2);
        map.clear();
        assertTrue(map.isEmpty());

    }

    @Test
    public void valuesTest() {
        MultiMap<String, String> map = new MultiMap<>();
        String value = "Values";
        String value2 = "Values2";
        String k1 = "k1";
        String k2 = "k2";
        String k3 = "k3";
        map.putEntry(value, k1, k2);
        map.putEntry(value2, k3);
        assertThat(map.values(), hasItem(value));
        assertThat(map.values(), hasItem(value2));
    }

    @Test
    public void keyTest() {
        MultiMap<String, String> map = new MultiMap<>();
        String value = "Values";
        String value2 = "Values2";
        String k1 = "k1";
        String k2 = "k2";
        String k3 = "k3";
        map.putEntry(value, k1, k2);
        map.putEntry(value2, k3);
        assertThat(map.keySet(), hasItem(new ComposedKey<>(k1, k2)));
        assertThat(map.keySet(), hasItem(new ComposedKey<>(k3)));
    }


    @Test
    public void sizeTest() {
        MultiMap<Integer, String> map = new MultiMap<>();
        map.put(new ComposedKey<>(1, 2), "abc");
        Integer[] key = {2, 3};
        map.put(key, "def");
        map.putEntry("def", 1, 2, 3, 4);
        assertThat(map.size(), is(3));
    }

    @Test
    public void isEmptyTest() {
        MultiMap<Integer, String> map = new MultiMap<>();
        assertTrue(map.isEmpty());
        map.put(new ComposedKey<>(1, 2), "abc");
        assertFalse(map.isEmpty());
    }

    @Test
    public void getTest() {
        String valueTest = "TestValue";
        MultiMap<Integer, String> map = new MultiMap<>();
        IntStream.range(1, 5).
                mapToObj(i -> new ComposedKey<>(i, i + 1))
                .forEach(i -> map.put(i, valueTest + i.toString()));
        assertThat(map.get(3, 4), is(valueTest + new ComposedKey<>(3, 4).toString()));
    }

    @Test
    public void putAllTest() {
        String valueTest = "TestValue";
        MultiMap<Integer, String> map = new MultiMap<>();
        IntStream.range(1, 5).
                mapToObj(i -> new ComposedKey<>(i, i + 1))
                .forEach(i -> map.put(i, valueTest + i));

        MultiMap<Integer, String> map2 = new MultiMap<>();
        IntStream.range(6, 10).
                mapToObj(i -> new ComposedKey<>(i, i + 1))
                .forEach(i -> map.put(i, valueTest + i));
        map.putAll(map2);

        map2.keySet().stream().forEach(k -> assertTrue(map.containsKey(k)));
        map2.keySet().stream().forEach(k -> assertTrue(map.containsValue(map2.get(k))));

    }

}
