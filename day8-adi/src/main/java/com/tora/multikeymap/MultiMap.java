package com.tora.multikeymap;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public class MultiMap<K, V> {
    HashMap<ComposedKey<K>, V> map;

    public MultiMap() {
        map = new HashMap<>();
    }

    private HashMap<ComposedKey<K>, V> getMap() {
        return map;
    }

    public int size() {
        return getMap().size();
    }

    public boolean isEmpty() {
        return getMap().isEmpty();
    }

    public boolean containsKey(K... key) {
        return containsKey(new ComposedKey<>(key));
    }

    public boolean containsKey(ComposedKey<K> key) {
        return keySet().stream()
                .anyMatch(k -> k.equals(key));
    }

    public boolean containsValue(V value) {
        return getMap().values().stream()
                .anyMatch(v -> v.equals(value));
    }

    public V get(K... key) {
        return get(new ComposedKey<>(key));
    }

    public V get(ComposedKey<K> key) {
        return getMap().get(key);
    }

    public V put(K[] keys, V value) {
        return put(new ComposedKey<>(keys), value);
    }

    public V putEntry(V value, K... keys) {
        return put(new ComposedKey<>(keys), value);
    }

    public V put(ComposedKey<K> key, V value) {
        return getMap().put(key, value);
    }

    public void putAll(MultiMap<K, V> m) {
        putAll(m.getMap());
    }

    public void putAll(HashMap<ComposedKey<K>, V> m) {
        getMap().putAll(m);
    }

    public void clear() {
        map = new HashMap<>();
    }

    public Set<ComposedKey<K>> keySet() {
        return map.keySet();
    }

    public Collection<V> values() {
        return getMap().values();
    }
}
