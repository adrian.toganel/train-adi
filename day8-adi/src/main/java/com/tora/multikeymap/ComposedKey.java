package com.tora.multikeymap;

import java.util.Arrays;

public class ComposedKey<T> {
    private T[] keys;

    public ComposedKey(T... keys) {
        this.keys = keys;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {return true;}
        if (o == null || getClass() != o.getClass()) {return false;}
        ComposedKey<?> that = (ComposedKey<?>) o;
        return Arrays.equals(keys, that.keys);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(keys);
    }

    @Override
    public String toString() {
        return "ComposedKey{" +
                "keys=" + Arrays.toString(keys) +
                '}';
    }
}
