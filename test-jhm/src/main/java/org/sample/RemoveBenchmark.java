package org.sample;

import com.tora.collections.InMemoryRepository;
import com.tora.orders.Order;

public class RemoveBenchmark extends GenericBenchmark {

    public void testMethod(InMemoryRepository inMemoryRepository, Order order) {
        inMemoryRepository.remove(order);
    }
}
