package org.sample;
/**
 * This can be used as state when running benchmark for class
 * CollectionBasedRepository
 */

import com.koloboke.collect.set.hash.HashObjSet;
import com.koloboke.collect.set.hash.HashObjSets;
import com.tora.collections.generic.CollectionBasedRepository;
import com.tora.orders.Order;
import gnu.trove.set.hash.THashSet;
import gnu.trove.set.hash.TLinkedHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectSortedSet;
import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.factory.Lists;
import org.eclipse.collections.impl.set.mutable.UnifiedSet;
import org.eclipse.collections.impl.set.sorted.mutable.TreeSortedSet;
import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@State(Scope.Benchmark)
public class MyState {

    static int NUMBER_OF_ORDERS = 100000;
    //JDK>
    CollectionBasedRepository<ArrayList> arrayListRepoJDK;
    CollectionBasedRepository<HashSet> hashSetRepoJDK;
    CollectionBasedRepository<TreeSet> treeSetRepoJDK;

    //Koloboke
    CollectionBasedRepository<HashObjSet> hashSetRepoKoloboke;

    //eclipse collections
    CollectionBasedRepository<MutableList> arrayListRepoEclipse;
    CollectionBasedRepository<UnifiedSet> hashSetRepoEclipse;
    CollectionBasedRepository<TreeSortedSet> treeSetRepoEclipse;

    //fast
    CollectionBasedRepository<ObjectArrayList> arrayListRepoFastUtils;
    CollectionBasedRepository<ObjectOpenHashSet> hashSetRepoFastUtils;

    //trove collections
    CollectionBasedRepository<THashSet> hashSetRepoFastTrove;
    CollectionBasedRepository<TLinkedHashSet> treeSetRepoFastTrove;

    @Setup(Level.Iteration)
    public void doSetup() {
        List<Order> listWithTestOrder = IntStream.range(0, NUMBER_OF_ORDERS)
                .mapToObj(i -> new Order(i, i, i))
                .collect(Collectors.toList());
        //jdk
        arrayListRepoJDK = new CollectionBasedRepository<>(new ArrayList<>(listWithTestOrder));
        hashSetRepoJDK = new CollectionBasedRepository<>(new HashSet<>(listWithTestOrder));
        treeSetRepoJDK = new CollectionBasedRepository<>(new TreeSet<>(listWithTestOrder));


        //koloboke
        hashSetRepoKoloboke = new CollectionBasedRepository<>(HashObjSets.newMutableSet(listWithTestOrder));

        //eclipse
        arrayListRepoEclipse = new CollectionBasedRepository<>(Lists.mutable.with(listWithTestOrder.toArray()));
        hashSetRepoEclipse = new CollectionBasedRepository<>(new UnifiedSet<>(listWithTestOrder));
        treeSetRepoEclipse = new CollectionBasedRepository<>(new UnifiedSet<>(listWithTestOrder));

        //fastutils
        arrayListRepoFastUtils = new CollectionBasedRepository<>(new ObjectArrayList<>(listWithTestOrder));
        hashSetRepoFastUtils = new CollectionBasedRepository<>(new ObjectOpenHashSet<>(listWithTestOrder));

        //trove4j
        hashSetRepoFastTrove = new CollectionBasedRepository<>(new THashSet<>(listWithTestOrder));
        treeSetRepoFastTrove = new CollectionBasedRepository<>(new TLinkedHashSet<>(listWithTestOrder));

    }
}
