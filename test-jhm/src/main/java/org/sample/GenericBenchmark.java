package org.sample;

import com.tora.collections.InMemoryRepository;
import com.tora.orders.Order;
import org.openjdk.jmh.annotations.*;

import java.util.Random;

public abstract class GenericBenchmark {

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 5, time = 1)
    public void testArrayJDK(MyState state) {
        testMethod(state.arrayListRepoJDK);

    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 5, time = 1)
    public void testHashSetJDK(MyState state) {
        testMethod(state.hashSetRepoJDK);

    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 5, time = 1)
    public void testTHashSetTROVE(MyState state) {
        testMethod(state.hashSetRepoFastTrove);

    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 5, time = 1)
    public void testTLinkedHashSetTroveMethod(MyState state) {
        testMethod(state.treeSetRepoFastTrove);

    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 5, time = 1)
    public void testTreeSetJDK(MyState state) {
        testMethod(state.treeSetRepoJDK);
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 5, time = 1)
    public void testObjectArrayListFastUtils(MyState state) {
        testMethod(state.arrayListRepoFastUtils);

    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 5, time = 1)
    public void testObjectOpenHashSetFastUtils(MyState state) {
        testMethod(state.hashSetRepoFastUtils);

    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 5, time = 1)
    public void testMutableListEclipse(MyState state) {
        testMethod(state.arrayListRepoEclipse);
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 5, time = 1)
    public void testHashObjSetKoloboke(MyState state) {
        testMethod(state.hashSetRepoKoloboke);
    }

    @Benchmark()
    @Fork(1)
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 5, time = 1)
    public void testUnifiedSetEclipse(MyState state) {
        testMethod(state.hashSetRepoEclipse);
    }

    @Benchmark()
    @Fork(1)
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 5, time = 1)
    public void testTreeSortedSetEclipse(MyState state) {
        testMethod(state.treeSetRepoEclipse);
    }

    /**
     * generates a random order and calls the abstract test method
     * initially the collection will have orders with ids 1,2,..,NUMBER_OF_ORDERS and a
     * the generated random orders with ids between 1-2*NUMBER_OF_ORDERS - half of the random orders will already be in the collection - for remove\contains operations
     *
     * @param inMemoryRepository the repository
     */
    private void testMethod(InMemoryRepository inMemoryRepository) {

        Order randomOrder = new Order(new Random().nextInt(MyState.NUMBER_OF_ORDERS * 2));
        testMethod(inMemoryRepository, randomOrder);
    }

    /**
     * should be overwritte with specific action remove, add, contains
     *
     * @param inMemoryRepository the repository
     * @param order an order
     */
    public abstract void testMethod(InMemoryRepository inMemoryRepository, Order order);
}
