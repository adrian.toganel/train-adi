package com.tora.orders;

import java.util.Objects;

public class Order implements Comparable {

    private int id;
    private int price;
    private int quantity;

    public Order(int i) {
        this.id = i;
        this.price = i;
        this.quantity = i;
    }
    public Order(int id, int price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id &&
                price == order.price &&
                quantity == order.quantity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, price, quantity);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        if (o == null){
            return 1;
        }
        if (this.equals(o)) {
            return 0;
        }
        if (o instanceof Order) {
            Order o2 = (Order) o;
            if (id > o2.id) {
                return 1;
            }
            if (quantity > o2.quantity) {
                return 1;
            }
            if (price > o2.price) {
                return 1;
            }
        }
        return -1;
    }
}
