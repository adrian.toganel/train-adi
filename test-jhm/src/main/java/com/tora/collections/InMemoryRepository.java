package com.tora.collections;

public interface InMemoryRepository {
    void add(Object t);

    boolean contains(Object t);

    void remove(Object t);

    int size();

    String getCollectionType();
}
