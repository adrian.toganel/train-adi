package com.tora.collections.generic;


import com.tora.collections.InMemoryRepository;

import java.util.Collection;

public class CollectionBasedRepository<T extends Collection> implements InMemoryRepository {
    private Collection list;

    public CollectionBasedRepository(Collection list) {
        this.list = list;
    }

    @Override
    public void add(Object order) {
        list.add(order);
    }

    @Override
    public boolean contains(Object order) {
        return list.contains(order);
    }

    @Override
    public void remove(Object order) {
        list.remove(order);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public String getCollectionType() {
        return list.getClass().getName();
    }
}
