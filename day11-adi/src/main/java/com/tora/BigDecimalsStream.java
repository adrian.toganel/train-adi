package com.tora;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class BigDecimalsStream {
    private List<BigDecimal> numberList;

    public BigDecimalsStream(List<BigDecimal> numberList) {
        this.numberList = numberList;
    }

    public BigDecimal getSum() {
        return numberList.stream()
                .filter(Objects::nonNull)
                .reduce(BigDecimal.ZERO,BigDecimal::add);
    }

    public BigDecimal getAverage() {
        return BigDecimal.valueOf(numberList.stream()
                .filter(Objects::nonNull)
                .mapToDouble(BigDecimal::doubleValue)
                .average().orElse(0.0));
    }

    public List<BigDecimal> biggestNumbers() {
        long elementsNumber = getSize();
        return numberList.stream().filter(Objects::nonNull)
                .sorted(Comparator.reverseOrder())
                .limit(1 + Double.valueOf(elementsNumber * 0.1).intValue())
                .collect(Collectors.toList());
    }

    private long getSize() {
        return numberList.stream().count();
    }

}
