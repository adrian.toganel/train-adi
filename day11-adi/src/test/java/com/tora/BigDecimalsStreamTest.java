package com.tora;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;


public class BigDecimalsStreamTest {

    @Test
    public void testSum() {
        List<BigDecimal> testList = Stream.of(20, -20.43, 22.2, 0.21, 121.2)
                .map(this::toBigDecimal).collect(Collectors.toList());
        assertThat(new BigDecimalsStream(testList).getSum(), is(toBigDecimal(143.18)));
    }

    @Test
    public void testAverage() {
        List<BigDecimal> testList = Stream.of(-23, 23, 82.6, -82.60, 0, 41, 9.9, -42.1)
                .map(this::toBigDecimal).collect(Collectors.toList());
        assertThat(new BigDecimalsStream(testList).getAverage(), is(closeTo(toBigDecimal(1.1), toBigDecimal(0.000001))));
    }

    @Test
    public void testGreater() {
        List<BigDecimal> testList = Stream.of(20, -20.43, 22.2, 0.21, 121.2)
                .map(this::toBigDecimal).collect(Collectors.toList());
        assertThat(new BigDecimalsStream(testList).biggestNumbers().size(), is(1));
        assertThat(new BigDecimalsStream(testList).biggestNumbers(), contains(toBigDecimal(121.2)));
    }

    @Test
    public void testNulls() {
        List<BigDecimal> testList = Stream.of(20, -20.43, 22.2, null, 0.21, 121.2)
                .map(this::toBigDecimal).collect(Collectors.toList());
        assertThat(new BigDecimalsStream(testList).biggestNumbers().size(), is(1));
        assertThat(new BigDecimalsStream(testList).biggestNumbers(), contains(toBigDecimal(121.2)));
    }


    @Test
    public void testGreater100() {
        List<BigDecimal> testList = IntStream.range(-100, 100)
                .mapToObj(this::toBigDecimal)
                .collect(Collectors.toList());
        List<BigDecimal> resultList = IntStream.range(79, 100)
                .mapToObj(this::toBigDecimal)
                .collect(Collectors.toList());
        assertThat(new BigDecimalsStream(testList).biggestNumbers().size(), is(21));
        assertThat(new BigDecimalsStream(testList).biggestNumbers(), containsInAnyOrder(resultList.toArray()));
    }

    @Test
    public void testEmptyList() {
        assertThat(new BigDecimalsStream(Arrays.asList()).biggestNumbers().size(), is(0));
        assertThat(new BigDecimalsStream(Arrays.asList()).getSum(), is(BigDecimal.valueOf(0)));
        assertThat(new BigDecimalsStream(Arrays.asList()).getAverage(), is(BigDecimal.valueOf(0.0)));
    }

    private BigDecimal toBigDecimal(Number n) {
        return n != null ? BigDecimal.valueOf(n.doubleValue()) : null;
    }
}
