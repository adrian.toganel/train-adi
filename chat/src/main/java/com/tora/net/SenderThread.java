package com.tora.net;

import com.tora.gui.MainPanel;

import java.io.*;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Queue;

public class SenderThread extends Thread {
    private final PrintWriter writer;
    private Socket socket;
    private Queue<String> messageQueue = new LinkedList<>();
    private String username;

    public SenderThread(Socket socket) throws IOException {
        this.socket = socket;
        OutputStream output = socket.getOutputStream();
        writer = new PrintWriter(output, true);
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void run() {
        String text = "";
        do {
            if (socket.isClosed()) return;
            if (messageQueue.size() != 0) {
                text = messageQueue.poll();
                writer.println("[" + username + "]" + text);
            }
        } while (!text.equals("bye"));

        try {
            close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void close() throws IOException {
        socket.close();
    }

    public void addToSenderQueue(String message) {
        messageQueue.offer(message);
    }
}
