package com.tora.net;

import com.tora.gui.MainPanel;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.io.*;
import java.net.Socket;

public class ReaderThread extends Thread implements ChangeListener {
    private final BufferedReader reader;
    private final Socket socket;
    private MainPanel mainPanel;

    public ReaderThread(Socket socket) throws IOException {
        this.socket = socket;
        InputStream input = socket.getInputStream();
        reader = new BufferedReader(new InputStreamReader(input));
    }

    public void setMainPanel(MainPanel mainPanel) {
        this.mainPanel = mainPanel;
    }

    public void run() {
        String text = "";
        try {
            while ((text != null) && !text.endsWith("]bye")) {
                if (socket.isClosed()) return;
                text = reader.readLine();
                mainPanel.addReceivedMessage(text);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void close() throws IOException {
        socket.close();
    }

    @Override
    public void stateChanged(ChangeEvent e) {

    }
}
