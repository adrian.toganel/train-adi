package com.tora.net;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class ConnectionPair {
    private SenderThread senderThread;
    private ReaderThread readerThread;

    public ConnectionPair(Socket socket) {
        try {
            senderThread = new SenderThread(socket);
            readerThread = new ReaderThread(socket);

        } catch (UnknownHostException ex) {
            System.out.println("Server not found: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("I/O Error: " + ex.getMessage());
        }
        readerThread.start();
        senderThread.start();

    }

    public SenderThread getSenderThread() {
        return senderThread;
    }

    public ReaderThread getReaderThread() {
        return readerThread;
    }

}
