package com.tora.gui;

import com.tora.net.ConnectionPair;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

public class MainPanel extends JFrame {

    private static final int DEFAULT_PORT = 4848;
    private static final String ACCEPTOR_CONNECT_BUTTON = "Start Connection";
    private static final String INITIATOR_CONNECT_BUTTON = "Connect";
    private static final String CONNECTED_TEXT = "Status: Connected";
    private static final String ACCEPTOR = "Acceptor";
    private static final String INITIATOR = "Initiator";
    private static final String NOT_CONNECTED_TEXT = "Status: NotConnected";

    private static final String[] roles = {ACCEPTOR, INITIATOR};
    private static final String HELLO_MESSAGE = "!hello";
    private static final String ACK_MESSAGE = "!ack";

    private static final TextField statusBar = new TextField(NOT_CONNECTED_TEXT);
    private final TextArea textArea = new TextArea();
    private final TextField messageField = new TextField();
    private final JButton sendMessageButton = new JButton("Send Message");
    private final Thread connectionCheckerThread;
    private ConnectionPair connections;

    private final JComboBox<String> roleCombobox = new JComboBox(roles);
    private final TextField userField = new TextField("Testuser");
    private final TextField hostField = new TextField("localhost");
    private final JSpinner portField = new JSpinner(new SpinnerNumberModel());
    private final JButton connectButton = new JButton(ACCEPTOR_CONNECT_BUTTON);
    private final AtomicBoolean isConnecting = new AtomicBoolean(false);

    public MainPanel() {
        this.setLayout(new BorderLayout());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(900, 200);
        initComponents();
        this.setVisible(true);
        //connection monitor thread
        connectionCheckerThread = new Thread(new ConnectionChecker());
    }

    private void setConnections(ConnectionPair connections) {
        this.connections = connections;
        connections.getReaderThread().setMainPanel(this);
        connections.getSenderThread().setUsername(userField.getText());
    }

    private void initComponents() {
        JPanel jpanel = new JPanel();
        sendMessageButton.addActionListener((e) -> sendMessage(messageField.getText()));

        jpanel.setLayout(new BorderLayout());
        jpanel.add(textArea, BorderLayout.CENTER);

        //message panel
        JPanel messagePanel = new JPanel(new BorderLayout());
        messagePanel.add(messageField, BorderLayout.CENTER);
        messagePanel.add(sendMessageButton, BorderLayout.EAST);
        jpanel.add(messagePanel, BorderLayout.SOUTH);

        this.getContentPane().add(jpanel, BorderLayout.CENTER);
        //connection panel
        this.getContentPane().add(getConnectionPanel(), BorderLayout.WEST);

    }

    private JPanel getConnectionPanel() {
        JPanel connectionPanel = new JPanel();

        portField.setValue(DEFAULT_PORT);
        statusBar.setEnabled(false);

        connectionPanel.add(new JLabel("Status: "));
        connectionPanel.add(statusBar);
        connectionPanel.add(new JLabel("Role"));
        roleCombobox.addActionListener(e -> {
            if (ACCEPTOR.equals(((JComboBox) e.getSource()).getSelectedItem())) {
                connectButton.setText(ACCEPTOR_CONNECT_BUTTON);
            } else {
                connectButton.setText(INITIATOR_CONNECT_BUTTON);
            }
        });
        connectionPanel.add(roleCombobox);
        connectionPanel.add(new JLabel("UserName"));
        connectionPanel.add(userField);
        connectionPanel.add(new JLabel("Host"));
        connectionPanel.add(hostField);
        connectionPanel.add(new JLabel("Port"));
        connectionPanel.add(portField);
        connectButton.addActionListener(e -> {
            try {
                initConnection();
            } catch (Exception ex) {
                statusBar.setText(NOT_CONNECTED_TEXT);
            }
        });
        connectionPanel.add(connectButton);

        connectionPanel.setLayout(new GridLayout(0, 2));
        return connectionPanel;
    }

    public void addReceivedMessage(String message) {
        SwingUtilities.invokeLater(() -> textArea.append("\n" + message));

    }

    private void sendMessage(String message) {
        if (!isConnected()) {
            textArea.append("\n ERROR: Cannot send message, not connected");
            return;
        }
        connections.getSenderThread().addToSenderQueue(message);
        textArea.append("\n[YOU]: " + message);
    }

    private void initConnection(){

        boolean isInitiator = INITIATOR.equals(roleCombobox.getSelectedItem());
        if (isConnecting.get()) {
            textArea.append("\nSYSTEM: Already trying to connect");
        }
        if (!isInitiator) {
            textArea.append("\nSYSTEM: Waiting for peer.....");
            isConnecting.set(true);
        }


        SwingWorker<Socket, String> connectionWorker = new SwingWorker<Socket, String>() {
            @Override
            protected Socket doInBackground() throws Exception {
                return isInitiator ? new Socket(hostField.getText(), (int) portField.getValue()) :
                        new ServerSocket((int) portField.getValue()).accept();
            }

            @Override
            protected void done() {
                try {
                    startConnecting(isInitiator, get());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        connectionWorker.execute();
    }

    private void startConnecting(boolean isInitiator, Socket socket) throws IOException {
        //send hello for initiator and receive ack,
        textArea.append("\n" + "SYSTEM: Connection initiated, starting communication protocol");
        try {
            initConnectionProtocol(isInitiator, socket);
        } catch (ConnectException e) {
            textArea.append("\n" + e.getMessage());
        }

        enableConnectionControls(false);
        setConnections(new ConnectionPair(socket));

        textArea.append("\nSYSTEM:" + CONNECTED_TEXT);
        statusBar.setText(CONNECTED_TEXT);
        isConnecting.set(false);
        connectionCheckerThread.start();
    }

    private void initConnectionProtocol(boolean isInitiator, Socket socket) throws IOException {
        textArea.append("\nSYSTEM: Starting connection protocol as " + (isInitiator ? INITIATOR : ACCEPTOR));
        if (isInitiator) {
            textArea.append("\nSYSTEM: Sending " + HELLO_MESSAGE);
            new PrintWriter(socket.getOutputStream(), true).println(HELLO_MESSAGE);
            textArea.append("\nSYSTEM: Waiting for " + ACK_MESSAGE);
            waitForReply(isInitiator, socket);
        } else {
            textArea.append("\nSYSTEM: Waiting for " + HELLO_MESSAGE);
            waitForReply(isInitiator, socket);
            textArea.append("\nSYSTEM: Sending " + ACK_MESSAGE);
            new PrintWriter(socket.getOutputStream(), true).println(ACK_MESSAGE);
        }
    }

    private void waitForReply(boolean isInitiator, Socket socket) throws IOException {
        if (isInitiator) {
            waitToReceiveMessage(socket, ACK_MESSAGE);
        } else {
            waitToReceiveMessage(socket, HELLO_MESSAGE);
        }
    }

    private void waitToReceiveMessage(Socket socket, String message) throws IOException {
        String reply = new BufferedReader(new InputStreamReader(socket.getInputStream())).readLine();
        if (!message.equals(reply))
            throw new ConnectException("Did not receive expected message : " + message + ", received" + reply);
    }


    private void enableConnectionControls(boolean enable) {
        roleCombobox.setEnabled(enable);
        userField.setEnabled(enable);
        hostField.setEnabled(enable);
        portField.setEnabled(enable);
        connectButton.setEnabled(enable);

    }

    private boolean isConnected() {
        return connections != null && connections.getReaderThread().isAlive() && connections.getSenderThread().isAlive();
    }

    class ConnectionChecker implements Runnable {

        @Override
        public void run() {
            while (isConnected()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            addReceivedMessage("CONNECTION CLOSED, Please restart application");
        }
    }
}
