package com.tora;

import com.tora.gui.MainPanel;

import javax.swing.*;
import java.io.IOException;


public class ChatMain {

    public static void main(String[] args) throws IOException {
        SwingUtilities.invokeLater(() -> new MainPanel());
    }
}
