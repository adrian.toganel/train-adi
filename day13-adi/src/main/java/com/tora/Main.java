package com.tora;

import com.google.common.collect.Lists;

import java.io.IOException;
import java.util.List;

public class Main {

    static List<String> files = Lists.newArrayList("day_13_input1.txt", "day13_input0.txt", "day13_input2.txt", "day13_input3.txt");
    static String inputLocation = "day13-adi/src/main/java/com/tora/input/";

    public static void main(String[] args) throws IOException {
        new ConvertFilesService(inputLocation,files).convertAll();
    }

}

