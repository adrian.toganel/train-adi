package com.tora.person;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class PersonValidator {


    private static boolean isValidName(String name) {
        return matches(name, ConstantsValidators.NAME_REGEXP);
    }

    private static boolean isValidCnp(String cnp) {
        return matches(cnp, ConstantsValidators.CNP_REGEXP);
    }

    private static boolean isValidEmail(String email) {
        return matches(email, ConstantsValidators.EMAIL_REGEXP);
    }

    private static boolean matches(String inputText, String regexp) {
        Pattern p = Pattern.compile(regexp);
        Matcher m = p.matcher(inputText);
        return m.find();
    }

    public static boolean isValid(Person person) {
        return person != null && isValidName(person.getFirstname()) && isValidName(person.getMiddlename())
                && isValidName(person.getSurname()) && isValidCnp(person.getPersonalNumeric()) &&
                isValidEmail(person.getEmail());
    }
}
