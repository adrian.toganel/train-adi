package com.tora.person;

import java.io.*;
import java.util.Objects;

public class Person implements Externalizable {
    private static final long serialVersionUID =  1L;
    private String firstname;
    private String middlename;
    private String surname;
    private String personalNumeric;
    private String email;

    public Person() {
    }

    public Person(String firstname1, String middlename, String surname, String personalNumeric, String email) {
        this.firstname = firstname1;
        this.middlename = middlename;
        this.surname = surname;
        this.personalNumeric = personalNumeric;
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public String getSurname() {
        return surname;
    }

    public String getPersonalNumeric() {
        return personalNumeric;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(firstname, person.firstname) &&
                Objects.equals(middlename, person.middlename) &&
                Objects.equals(surname, person.surname) &&
                Objects.equals(personalNumeric, person.personalNumeric) &&
                Objects.equals(email, person.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstname, middlename, surname, personalNumeric, email);
    }


    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeLong(serialVersionUID);
        out.writeUTF(getFirstname());
        out.writeUTF(getMiddlename());
        out.writeUTF(getSurname());
        out.writeUTF(getPersonalNumeric());
        out.writeUTF(getEmail());
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        long version = in.readLong();

        if (version == 1L){
            firstname = in.readUTF();
            middlename = in.readUTF();
            surname = in.readUTF();
            personalNumeric = in.readUTF();
            email = in.readUTF();
        }

    }
}
