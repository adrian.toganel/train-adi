package com.tora.person;

public class ConstantsValidators {
    public static String NAME_REGEXP = "^[A-Z]{1}[a-z]+$";
    public static String CNP_REGEXP = "^\\d{13}$";
    public static String EMAIL_REGEXP = "^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$";

    public static String PERSON_SEPARATOR = "#";
    public static String FIELDS_SEPARATOR = "\\|";

    public static int INDEX_FIRSTNAME = 0;
    public static int INDEX_MIDDLENAME = 1;
    public static int INDEX_SURNAME = 2;
    public static int INDEX_CNP = 3;
    public static int INDEX_EMAIL = 4;
}
