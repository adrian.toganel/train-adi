package com.tora;

import com.tora.file.FileParserClientFormat;
import com.tora.file.WriterPersonFormatUtils;
import com.tora.person.Person;

import java.io.IOException;
import java.util.List;

public class ConvertFilesService {
    private final String inputLocation;
    private final List<String> files;

    public ConvertFilesService(String inputLocation, List<String> files) {
        this.inputLocation = inputLocation;
        this.files = files;
    }

    public void convertAll() {
        for (String s : files) {
            try {
                convertFile(s);
            } catch (IOException e) {
                System.err.println("The following file cannot be converter : " + s);
                e.printStackTrace();
            }
        }
    }

    private void convertFile(String s) throws IOException {
        System.out.println("**********\nFILE: " + s);
        String filename = inputLocation + s;
        List<Person> validPersons = new FileParserClientFormat(filename).getValidPersons();
        System.out.println("Valid persons number: " + validPersons.size());
        System.out.println("Invalid persons number: " + new FileParserClientFormat(filename).getInvalidPersons().size());
        String serializedFile = filename + "_serialized.txt";
        System.out.println("Saving in serialized format in file " + serializedFile);
        WriterPersonFormatUtils.writePersons(validPersons, serializedFile);
    }
}
