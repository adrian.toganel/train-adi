package com.tora.conccurency;

import com.google.common.collect.Lists;
import com.tora.person.Person;

import java.util.List;

public class MyRepository {

    private List<Person> personList = Lists.newArrayList();

    public void addAll(List<Person> personsToBeAdded) {
        personList.addAll(personsToBeAdded);
    }

    public List<Person> getPersonList() {
        return personList;
    }

    public int size() {
        return personList.size();
    }

}
