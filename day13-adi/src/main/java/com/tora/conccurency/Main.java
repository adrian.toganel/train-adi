package com.tora.conccurency;

import com.google.common.collect.Lists;
import com.tora.file.FileParserClientFormat;
import com.tora.person.Person;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public class Main {
    private static final MyRepository myRepoSingleThread = new MyRepository();
    private static final MyRepository myRepoMultiThread = new MyRepository();
    private static final Object objectLock = new Object();

    private static List<String> files = Lists.newArrayList("day_13_input1.txt", "day_13_input1.txt", "day_13_input1.txt", "day_13_input1.txt", "day_13_input1.txt", "day13_input0.txt", "day13_input2.txt", "day13_input4.txt", "day13_input5.txt", "day13_input6.txt");
    private static String inputLocation = "day13-adi/src/main/java/com/tora/input/";

    public static void main(String[] args) throws IOException, InterruptedException {
        long start = new Date().getTime();
        addSingleThead();
        System.out.println("Single thread read  ended after: " + (new Date().getTime() - start));
        System.out.println("Single thread populated repo size:" + myRepoSingleThread.size());

        start = new Date().getTime();
        addMultiThread();
        System.out.println("Multi thread read  ended after: " + (new Date().getTime() - start));
        System.out.println("Multi thread populated repo size:" + myRepoMultiThread.size());
    }

    /**
     * create a thread for each file and add the result to myRepoMultiThread
     */

    private static void addMultiThread() throws InterruptedException {
        List<Thread> threads = Lists.newArrayList();
        for (String s : files) {
            final Runnable task = () -> {
                List<Person> personList = new FileParserClientFormat(inputLocation + s).getValidPersons();
                synchronized (objectLock) {
                    myRepoMultiThread.addAll(personList);
                }
            };
            threads.add(new Thread(task));
        }

        for (Thread thread : threads) {
            thread.start();
        }

        for (Thread thread : threads) {
            thread.join();
        }
    }

    /**
     * read each file on same thread and content to myRepoMultiThread
     */

    public static void addSingleThead() throws IOException {
        for (String s : files) {
            String fileName = inputLocation + s;
            myRepoSingleThread.addAll(new FileParserClientFormat(fileName).getValidPersons());
        }
    }


}
