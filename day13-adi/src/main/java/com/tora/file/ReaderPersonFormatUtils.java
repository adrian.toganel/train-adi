package com.tora.file;

import com.tora.person.Person;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class ReaderPersonFormatUtils {
    public static List<Person> readPersons(String filename) throws IOException, ClassNotFoundException {
        FileInputStream file = new FileInputStream(filename);
        ObjectInputStream in = new ObjectInputStream(file);
        List<Person> personList = new ArrayList<>();
        Person p = null;
        boolean stop = false;
        while (!stop) {
            try {
                p = (Person) in.readObject();
            } catch (EOFException e) {
                p = null;

            }
            if (p != null) personList.add(p);
            else stop = true;
        }
        in.close();
        file.close();
        return personList;

    }
}
