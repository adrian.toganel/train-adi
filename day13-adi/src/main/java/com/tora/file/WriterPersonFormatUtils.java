package com.tora.file;

import com.tora.person.Person;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

/***
 * This class is an utils class that can be used to write Person objects in a file
 */
public class WriterPersonFormatUtils {

    public static void writePersons(List<Person> personList, String filename) throws IOException {
        FileOutputStream file = new FileOutputStream (filename);
        ObjectOutputStream out = new ObjectOutputStream (file);

        // Method for serialization of object
        for (Person person : personList){
            out.writeObject(person);
        }

        out.close();
        file.close();
    }


}