package com.tora.file;


import com.google.common.collect.Lists;
import com.tora.person.Person;
import com.tora.person.PersonValidator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.tora.person.ConstantsValidators.*;

public class FileParserClientFormat {

    private String filePath;

    public FileParserClientFormat(String filePath) {
        this.filePath = filePath;
    }

    private List<String> extractPersonList() {
        BufferedReader bf = null;
        try {
            bf = new BufferedReader(new FileReader(filePath));
        } catch (FileNotFoundException e) {
            return Lists.newArrayList();
        }
        String line;
        StringBuilder sb = new StringBuilder();
        try {
            while ((line = bf.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e){
            return Lists.newArrayList();
        }

        return Arrays.asList(sb.toString().split(PERSON_SEPARATOR));
    }

    public List<Person> getValidPersons() {
        return extractPersonList().stream()
                .filter(this::isValidCandidate)
                .map(this::getPerson)
                .filter(PersonValidator::isValid)
                .collect(Collectors.toList());
    }

    public List<Person> getInvalidPersons() throws IOException {
        return extractPersonList().stream()
                .map(this::getPerson)
                .filter(person -> !PersonValidator.isValid(person))
                .collect(Collectors.toList());
    }

    private boolean isValidCandidate(String input) {
        return INDEX_EMAIL + 1 == input.split(FIELDS_SEPARATOR).length;
    }

    private Person getPerson(String input) {
        Person person;
        List<String> fields = Arrays.stream(input.split(FIELDS_SEPARATOR))
                .map(String::trim)
                .collect(Collectors.toList());
        if (fields.size() == INDEX_EMAIL + 1) {
            person = new Person(fields.get(INDEX_FIRSTNAME), fields.get(INDEX_MIDDLENAME), fields.get(INDEX_SURNAME), fields.get(INDEX_CNP), fields.get(INDEX_EMAIL));
        } else {
            person = null;
        }
        return person;
    }

}


