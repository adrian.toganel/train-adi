package com.tora.person;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class PersonTest {

    public static String firstname1 = "John";
    public static String firstname2 = "Johnny";
    public static String surName = "Doe";
    public static String surName2 = "DoeTwo";
    public static String cnp = "1430611013773";
    public static String cnp2 = "1430611013774";
    public static String email = "john.doe@gmail.com";

    @Test
    void newPerson() {
        Person p = getJohnDoe();
        assertThat(p.getFirstname(), is(firstname1));
        assertThat(p.getMiddlename(), is(firstname2));
        assertThat(p.getSurname(), is(surName));
        assertThat(p.getPersonalNumeric(), is(cnp));
        assertThat(p.getEmail(), is(email));
    }

    public static Person getJohnDoe() {
        return new Person(firstname1, firstname2, surName, cnp, email);
    }

    public static Person getJohnDoe2() {
        return new Person(firstname1, firstname2, surName2, cnp2, email);
    }
}
