package com.tora.person;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PersonValidatorTest {
    @Test
    public void validPerson(){
        Person p = PersonTest.getJohnDoe();
        assertTrue(PersonValidator.isValid(p));
    }
    @Test
    public void invalidFirstName(){
        String firstname1 = "Joh1n";
        String firstname2 = "Johnny";
        String surName = "Doe";
        String cnp = "1430611013773";
        String email = "john.doe@gmail.com";
        Person p = new Person(firstname1,firstname2,surName,cnp,email);
        assertFalse(PersonValidator.isValid(p));
    }
    @Test
    public void invalidMiddle(){
        String firstname1 = "John";
        String firstname2 = "johnny";
        String surName = "Doe";
        String cnp = "1430611013773";
        String email = "john.doe@gmail.com";
        Person p = new Person(firstname1,firstname2,surName,cnp,email);
        assertFalse(PersonValidator.isValid(p));
    }
    @Test
    public void invalidLast(){
        String firstname1 = "John";
        String firstname2 = "Johnny";
        String surName = "DOe";
        String cnp = "1430611013773";
        String email = "john.doe@gmail.com";
        Person p = new Person(firstname1,firstname2,surName,cnp,email);
        assertFalse(PersonValidator.isValid(p));
    }
    @Test
    public void invaliCNP(){
        String firstname1 = "John";
        String firstname2 = "Johnny";
        String surName = "Doe";
        String cnp = "14306110213773";
        String email = "john.doe@gmail.com";
        Person p = new Person(firstname1,firstname2,surName,cnp,email);
        assertFalse(PersonValidator.isValid(p));
    }
    @Test
    public void invalidMail(){
        String firstname1 = "John";
        String firstname2 = "Johnny";
        String surName = "Doe";
        String cnp = "1430611013773";
        String email = "john.doe@gmailcom";
        Person p = new Person(firstname1,firstname2,surName,cnp,email);
        assertFalse(PersonValidator.isValid(p));
    }
}
