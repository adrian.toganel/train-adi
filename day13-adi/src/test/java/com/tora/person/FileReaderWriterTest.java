package com.tora.person;

import com.google.common.collect.Lists;
import com.tora.file.ReaderPersonFormatUtils;
import com.tora.file.WriterPersonFormatUtils;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class FileReaderWriterTest {

    @Test
    public void writeTest () throws IOException, ClassNotFoundException {
        String fileName = "test.txt";

        Person johnDoe = PersonTest.getJohnDoe();
        Person johnDoe2 = PersonTest.getJohnDoe2();
        //write john doe to file
        WriterPersonFormatUtils.writePersons(Lists.newArrayList(johnDoe,johnDoe2),fileName);

        //
        List<Person> persons = ReaderPersonFormatUtils.readPersons(fileName);
        assertThat(persons.size(),is(2));
        assertThat(persons.get(0),is(johnDoe));
        assertThat(persons.get(1),is(johnDoe2));
    }

    @Test
    public void test() throws IOException, ClassNotFoundException {
        List<Person> persons = ReaderPersonFormatUtils.readPersons("test.txt");
        assertThat(persons.size(),is(2));
        assertThat(persons.get(0),is(PersonTest.getJohnDoe()));
        assertThat(persons.get(1),is(PersonTest.getJohnDoe2()));
    }

}
