package org.sample;

import org.openjdk.jmh.annotations.*;
import tora.com.lines.CustomFileWalk;
import tora.com.lines.CustomReadLinesUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class BenchmarkTest {
    static Path path = Paths.get("src/test/java/tora/com/testfiles");
    static Path filePath = Paths.get("src/test/java/tora/com/testfiles/day13_input3-errors.txt");

    @Benchmark
    @Fork(5)
    @Warmup(iterations = 3, time = 1)
    @Measurement(iterations = 10, time = 1)
    public void testNioLines() throws IOException {
        Files.lines(filePath);

    }

    @Benchmark
    @Fork(5)
    @Warmup(iterations = 3, time = 1)
    @Measurement(iterations = 10, time = 1)
    public void testNioWalk() throws IOException {
        Files.walk(path);
    }

    @Benchmark
    @Fork(5)
    @Warmup(iterations = 3, time = 1)
    @Measurement(iterations = 10, time = 1)
    public void testCustomLines() throws IOException {
        CustomReadLinesUtils.lines(filePath);
    }

    @Benchmark
    @Fork(5)
    @Warmup(iterations = 3, time = 1)
    @Measurement(iterations = 10, time = 1)
    public void testCustomWalk() throws IOException {
        CustomFileWalk.walk(path);
    }
}
