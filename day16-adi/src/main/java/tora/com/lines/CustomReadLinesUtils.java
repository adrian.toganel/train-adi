package tora.com.lines;

import com.google.common.collect.Lists;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

public class CustomReadLinesUtils {

    public static Stream<String> lines(Path path) throws IOException {
        BufferedReader reader = null;
        List<String> result = Lists.newArrayList();

        try {
            reader = new BufferedReader(new FileReader(path.toAbsolutePath().toFile()));
        } catch (FileNotFoundException e) {
            return result.stream();
        }

        String line;
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();
        return result.stream();

    }
}
