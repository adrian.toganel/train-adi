package tora.com.lines;

import com.google.common.collect.Lists;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CustomFileWalk {
    public static Stream<Path> walk(Path filePath) {
        List<Path> result = Lists.newArrayList();
        File currentFile = new File(filePath.toUri());
        result.add(Paths.get(System.getProperty("user.dir")).relativize(currentFile.toPath()));
        if (currentFile.isFile()) {
            return result.stream();
        }

        for (File file : currentFile.listFiles()) {
            result.addAll(walk(file.toPath()).collect(Collectors.toList()));
        }
        return result.stream();
    }
}
