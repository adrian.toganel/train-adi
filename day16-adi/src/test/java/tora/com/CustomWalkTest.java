package tora.com;

import org.junit.Test;
import tora.com.lines.CustomFileWalk;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

public class CustomWalkTest {
    public static final String testFilesLocation = "src/test/java/tora/com/testfiles";

    @Test
    public void walkTest() throws IOException {
        Path path = Paths.get(testFilesLocation);
        List<Path> myResult = CustomFileWalk.walk(path).collect(Collectors.toList());

        List<Path> nioResult = Files.walk(path).collect(Collectors.toList());

        assertThat(myResult.size(), is(nioResult.size()));
        assertThat(nioResult, contains(myResult.toArray()));
    }
}
