package tora.com;

import org.junit.Test;
import tora.com.lines.CustomReadLinesUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

public class LinesTest {
    private static final String testFilesLocation = "src/test/java/tora/com/testfiles";

    @Test
    public void readLinesTest() throws IOException {
        String testFile = "testLines.txt";
        Path path = Paths.get(testFilesLocation + "/" + testFile);
        List<String> myResult = CustomReadLinesUtils.lines(path).collect(Collectors.toList());

        List<String> nioResult = Files.lines(path).collect(Collectors.toList());

        assertThat(myResult.size(), is(nioResult.size()));
        assertThat(nioResult, contains(myResult.toArray()));
    }
}
