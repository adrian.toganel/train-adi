package com.tora;

import java.util.List;

public class CustomStringJoiner {


    String string = "";
    String delimiter; //default is empty space


    public String getString() {
        return string;
    }

    public CustomStringJoiner(String token) {
        this.delimiter = token;
    }

    public void setDelimiter(String token) {
        this.delimiter = token;
    }

    public void addToken(String stringToAdd) {
        StringBuilder sb = new StringBuilder();
        string = sb.append(string).append(stringToAdd).append(delimiter).toString();
    }

    public void removeToken(String token) {
        List<String> allTokens = new CustomStringTokenizer(string,delimiter).getTokens();
        string = "";
        for (String s : allTokens){
            if (!s.equals(token)) addToken(s);
        }
    }
}
