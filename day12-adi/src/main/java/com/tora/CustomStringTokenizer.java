package com.tora;


import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.List;

public class CustomStringTokenizer {
    String string;
    String delimiter; //default is empty space
    List<String> tokenList = Lists.newArrayList();


    public CustomStringTokenizer(String string, String token) {
        this.string = string;
        this.delimiter = token;
        refreshTokenList();
    }

    public void setString(String string) {
        this.string = string;
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    public CustomStringTokenizer(String string) {
        this(string, " ");
    }

    private void refreshTokenList(){
        tokenList =  Lists.newArrayList();
        int index = 0;
        int lastTokenIndex = 0;
        while (index < string.length()){
            int initialIndex = index;
            while (index < string.length() && delimiter.indexOf(string.charAt(index)) >= 0)
            {
                index++;
            }
            if (index != initialIndex){
                tokenList.add(string.substring(lastTokenIndex,initialIndex));
                lastTokenIndex = index;
            }
            else index ++;
        }

        tokenList.remove("");
        //not mathces
        if (tokenList.isEmpty()) tokenList.add(string);
    }

    public int countTokens() {
        return getTokens().size();
    }

    public List<String> getTokens() {
        return tokenList;
    }

    public String getTokenAt(int index) {
        if (index > countTokens()) {
            throw new IndexOutOfBoundsException("Token with index " + index + " does not exist. Tokens count is " + countTokens());
        }
        return getTokens().get(index);
    }
}
