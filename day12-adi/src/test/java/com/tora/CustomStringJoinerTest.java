package com.tora;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CustomStringJoinerTest {
    @Test
    public void addToken(){
        String string = "string1";
        String string2 = "string2";
        String delimiter = "ZZ";
        CustomStringJoiner customJoiner = new CustomStringJoiner(delimiter);
        customJoiner.addToken(string);
        assertThat(customJoiner.getString(), is(string+delimiter));
        customJoiner.addToken(string2);
        assertThat(customJoiner.getString(), is(string+delimiter+string2+delimiter));
    }

    @Test
    public void remove(){
        String string = "string1";
        String string2 = "string2";
        String string3 = "string3";
        String delimiter = "ZZ";
        CustomStringJoiner customJoiner = new CustomStringJoiner(delimiter);
        customJoiner.addToken(string);
        customJoiner.addToken(string2);
        customJoiner.addToken(string3);
        customJoiner.removeToken(string2);
        assertThat(customJoiner.getString(), is(string+delimiter+string3+delimiter));
    }
}
