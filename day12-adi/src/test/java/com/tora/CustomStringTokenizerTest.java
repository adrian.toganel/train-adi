package com.tora;

import org.junit.Test;

import java.util.List;
import java.util.StringTokenizer;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class CustomStringTokenizerTest {

    @Test
    public void countTokensTest() {
        assertThat(new CustomStringTokenizer("aBc test string AbcaBc token is aBc", ";").countTokens(), is(new StringTokenizer("aBc test string AbcaBc token is aBc", ";").countTokens()));
        assertThat(new CustomStringTokenizer("aBc test string AbcaBc token is aBc", "aBc").countTokens(), is(new StringTokenizer("aBc test string AbcaBc token is aBc", "aBc").countTokens()));
        assertThat(new CustomStringTokenizer("1 zzz 2 zzz3zzz 4zzz5zzz 6zzz 7zzz 8zzz 9zzz", "zzz").countTokens(), is(new StringTokenizer("1 zzz 2 zzz3zzz 4zzz5zzz 6zzz 7zzz 8zzz 9", "zzz").countTokens()));

    }

    @Test
    public void getTokensTest() {
        String string = "zzz1 zzz 2 zzz3zzz 4zzz5zzz 6zzz 7zzz 8zzz 9zzz asd#@%zzzsadzzz";
        String delimiter = "zzz";

        List<String> mytokens = new CustomStringTokenizer(string, delimiter).getTokens();

        StringTokenizer stringTokenizer = new StringTokenizer(string, delimiter);
        int count = stringTokenizer.countTokens();
        assertThat(count, is(mytokens.size()));
        while (stringTokenizer.hasMoreTokens()) {
            String tocheck = stringTokenizer.nextToken();
            assertThat(mytokens, hasItem(tocheck));
        }
    }

    @Test
    public void getTokensAtTest() {
        String string = "zyz1 zyz 2 zyz3zyz 4zyz5zyz 6z 7zyzasdzyz 8zyz 9zyz asd#@%zyzsadzyz";
        String delimiter = "zyz";
        int atIndex = 6;

        CustomStringTokenizer customStringTokenizer = new CustomStringTokenizer(string, delimiter);

        StringTokenizer stringTokenizer = new StringTokenizer(string, delimiter);
        int index = 0;
        String stringFromStandartTokenizer = "";

        while (stringTokenizer.hasMoreTokens() && index <= atIndex) {
            stringFromStandartTokenizer = stringTokenizer.nextToken();
            index++;
        }
        assertThat(stringFromStandartTokenizer, is(customStringTokenizer.getTokenAt(atIndex)));
    }

    @Test
    public void getTokensAtRegExpTest() {
        String string = "zzz1 . 2 zzz3zzz 4zzz5zzz 6zzzzzzzzz 7zzasdz . 9zzz asd#@%zzzsad.zzzz";
        String delimiter = ".";
        int atIndex = 2;

        CustomStringTokenizer customStringTokenizer = new CustomStringTokenizer(string, delimiter);

        StringTokenizer stringTokenizer = new StringTokenizer(string, delimiter);
        int index = 0;
        String stringFromStandartTokenizer = "";

        while (stringTokenizer.hasMoreTokens() && index <= atIndex) {
            stringFromStandartTokenizer = stringTokenizer.nextToken();
            index++;
        }
        assertThat(stringFromStandartTokenizer, is(customStringTokenizer.getTokenAt(atIndex)));
    }
}
