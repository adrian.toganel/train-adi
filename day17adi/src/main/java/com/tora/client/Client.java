package com.tora.client;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.net.ConnectException;
import java.util.concurrent.TimeoutException;

public class Client {
    private final String host;
    final String queue;
    Channel channel;


    public Client(String queue) throws ConnectException {
        this("localhost",queue);
    }

    public Client(String host, String queue) throws ConnectException {
        this.host = host;
        this.queue = queue;
        try {
            initChannel();
        } catch (IOException | TimeoutException e) {
            throw new ConnectException("cannot initiate channel");
        }
    }

    private void initChannel() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        Connection connection = factory.newConnection();
        channel = connection.createChannel();

        channel.queueDeclare(queue, false, false, false, null);
    }

}
