package com.tora.client;

import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.net.ConnectException;
import java.time.LocalDateTime;

public class Receiver extends Client {
    public Receiver(String queue) throws ConnectException {
        super(queue);
    }
    public Receiver(String host, String queue) throws ConnectException {
        super(host, queue);
    }

    public void printMessages() throws IOException {
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(LocalDateTime.now() + " [x] Received on queue '" + queue + ", message" + message + "'");
        };
        super.channel.basicConsume(queue, true, deliverCallback, consumerTag -> { });

    }
}
