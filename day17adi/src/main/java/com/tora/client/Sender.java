package com.tora.client;

import java.io.IOException;
import java.net.ConnectException;
import java.nio.charset.StandardCharsets;

public class Sender extends Client {
    public Sender(String queue) throws ConnectException {
        super(queue);
    }

    public Sender(String host, String queue) throws ConnectException {
        super(host, queue);
    }

    public void sendMessage(String message) throws IOException {
        channel.basicPublish("", queue, null, message.getBytes(StandardCharsets.UTF_8));
        System.out.println(" [x] Sent '" + message + "'");
    }
}
